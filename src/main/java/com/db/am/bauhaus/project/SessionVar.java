package com.db.am.bauhaus.project;

/**
 * Created by ongshir on 06/04/2017.
 */
public enum SessionVar {
    INPUT_SEARCH_TEXT,
    SEARCH_SUGGESTIONS,
    SELECTED_CATEGORY,
    SELECTED_SUBCATEGORY,
    REGISTERED_USER,
    LOGGED_IN_USER_ID,
    SELECTED_PRODUCT,
    DATA_NODE_ID;
}
