package com.db.am.bauhaus.project.util;

import com.db.am.bauhaus.project.bo.User;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Map;
import java.util.Random;

public class GenericHelper {

    private static final String firstName = "testFName";

    public static User generateRandomUser(){
        String randomPassword = getRandomString(6);
        return populateUser(firstName, getRandomEmail(), randomPassword, randomPassword);
    }

    private static String getRandomEmail(){
        StringBuilder sb = new StringBuilder();
        return sb.append(getRandomString(10)).append("@").append(getRandomString(4)).append(".com").toString();
    }
    public static int getRandomNumber(int maxLength) {
        return new Random().nextInt(maxLength);
    }

    public static String getRandomString(int maxLength) {
         return RandomStringUtils.randomAlphanumeric(maxLength);
    }

    public static User populateUser(String firstName, String email, String password, String repeatPassword) {
        User user = new User();
        user.setEmailId(email);
        user.setFirstName(firstName);
        user.setPassword(password);
        user.setRepeatPassword(repeatPassword);
        return user;
    }
}
