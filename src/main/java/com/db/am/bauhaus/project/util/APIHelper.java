package com.db.am.bauhaus.project.util;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.apache.commons.lang3.RandomStringUtils;

public class APIHelper {
    private static final String baseUri = getSerenityProperty("webdriver.base.url");
    private static final String API_KEY_SEARCH_SHOP_NAME = "searchShopName";
    private static final String API_KEY_CHECK_USERNAME = "checkUserNameAvailability";
    private static final String API_KEY_SUGGEST_USERNAME = "suggestUserName";

    public static String getSearchShopNameEndPoint() {
        return baseUri + getSerenityProperty(API_KEY_SEARCH_SHOP_NAME);
    }

    public static String getCheckUsernameAvailabilityEndPoint() {
        return baseUri + getSerenityProperty(API_KEY_CHECK_USERNAME);
    }

    public static String getSuggestUsernameEndPoint() {
        return baseUri + getSerenityProperty(API_KEY_SUGGEST_USERNAME);
    }

    private static String getSerenityProperty(String key) {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return variables.getProperty(key);
    }
}
