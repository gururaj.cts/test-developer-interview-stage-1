package com.db.am.bauhaus.project.util;

import com.db.am.bauhaus.project.SessionVar;
import net.serenitybdd.core.Serenity;

public class SessionHelper {

    public static <T> T get(SessionVar key){
        return Serenity.sessionVariableCalled(key);
    }

    public static <T> void set(T value, SessionVar key){
        Serenity.setSessionVariable(key).to(value);
    }
}
