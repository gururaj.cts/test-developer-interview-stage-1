package com.db.am.bauhaus.project.bo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class User {

    private String firstName;
    private String emailId;
    private String password;
    private String repeatPassword;

}
