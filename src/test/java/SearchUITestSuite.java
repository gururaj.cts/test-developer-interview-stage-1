import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/ui/Search.feature", format = {"json:target/cucumber-report/runjenkins/SearchUITestSuite.json", "pretty"}, monochrome = true, strict=true, junit = "--filename-compatible-names")
public class SearchUITestSuite {
}
