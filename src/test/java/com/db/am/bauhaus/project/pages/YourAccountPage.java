package com.db.am.bauhaus.project.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class YourAccountPage extends PageObject {

    @FindBy(css = "a[title='Your account']")
    private WebElementFacade yourAccountLink;

    @FindBy(css = ".details .name")
    private WebElementFacade accountName;

    public void goToYourAccounts() {
        yourAccountLink.click();
    }

    public String getAccountName() {
        return accountName.getText();
    }
}
