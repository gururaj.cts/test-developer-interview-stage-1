package com.db.am.bauhaus.project.steps;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.pages.*;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by ongshir on 05/10/2016.
 */
public class CategorySteps extends ScenarioSteps {

    CategoryPage categoryPage;

    SubCategoryPage subCategoryPage;

    SearchResultsPage searchResultsPage;

    HomePage homePage;

    ProductDetailsPage productDetailsPage;

    @Step
    public void navigateToSubCategory(String category, String subCategory) throws InterruptedException {
        homePage.navigateToCategory(category);
        categoryPage.navigateToSubCategory(subCategory);
    }

    @Step
    public void verifySubCategoryPageResults() {
        String pageTitle = subCategoryPage.getTitle();
        String breadCrumb = subCategoryPage.getBreadCrumb();
        String subCategoryHeader = subCategoryPage.getSubCategoryHeader();
        String selectedCategory = SessionHelper.get(SessionVar.SELECTED_CATEGORY);
        String selectedSubCategory = SessionHelper.get(SessionVar.SELECTED_SUBCATEGORY);

        assertThat(breadCrumb.contains(selectedCategory), is(true));
        assertThat(breadCrumb.contains(selectedSubCategory), is(true));
        assertThat(pageTitle.contains(selectedSubCategory), is(true));
        assertThat(subCategoryHeader.contains(selectedSubCategory), is(true));
    }

    @Step
    public void selectProduct(String productName) {
        searchResultsPage.selectProduct(productName);
    }

    public void verifyProductDetails() {
        assertThat(productDetailsPage.getProductName().contains(SessionHelper.get(SessionVar.SELECTED_PRODUCT)), is(true));
    }
}
