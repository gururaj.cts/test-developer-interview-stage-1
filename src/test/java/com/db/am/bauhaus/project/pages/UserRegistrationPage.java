package com.db.am.bauhaus.project.pages;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.bo.User;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class UserRegistrationPage extends Page {

    @FindBy(id = "first-name")
    private WebElementFacade firstName;

    @FindBy(id = "email")
    private WebElementFacade email;

    @FindBy(id = "password")
    private WebElementFacade password;

    @FindBy(id = "password-repeat")
    private WebElementFacade passwordRepeat;

    @FindBy(id = "register_button")
    private WebElementFacade registerButton;

    private By firstNameError = By.id("first-name-error");

    private By emailError = By.id("email-error");

    private By passwordError = By.id("password-error");

    private By repeatPasswordError = By.id("password-repeat-error");

    public void registerUser(User user) {
        firstName.sendKeys(user.getFirstName());
        email.sendKeys(user.getEmailId());
        password.sendKeys(user.getPassword());
        passwordRepeat.sendKeys(user.getRepeatPassword());
        SessionHelper.set(user, SessionVar.REGISTERED_USER);
        registerButton.click();
    }

    public boolean validateErrorScenario(String scenario, boolean isFail){
        By identifier = getValidationErrorFieldIdentifier(scenario);
        boolean valid = false;
        if(identifier != null) {
            if (isFail) {
                shouldBeVisible(identifier);
                valid = element(identifier).getText().equalsIgnoreCase("Can't be blank.");
            } else {
                shouldNotBeVisible(identifier);
                valid = true;
            }
        }
        return valid;
    }

    private By getValidationErrorFieldIdentifier(String scenario){
        switch (scenario){
            case "firstNameBlank": return firstNameError;
            case "emailBlank": return emailError;
            case "passwordBlank": return passwordError;
            case "reenterPasswordBlank": return repeatPasswordError;
            default: return null;
        }
    }
}
