package com.db.am.bauhaus.project.api;

import com.db.am.bauhaus.project.util.APIHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;

import static org.hamcrest.core.Is.is;

public class SearchShopName extends APIWrapper {


    @Given("^Search shop name API is available$")
    public void apiToSearchShopNameIsAvailable() {
        buildUriSpec(APIHelper.getSearchShopNameEndPoint());
    }

    @When("^I make shop name search for input product \"([^\"]*)\"$")
    public void searchShopNames(String productName) throws Throwable {
        addRequestParam("q", productName);
        callService();
    }

    @Then("^I get response with valid shop name results$")
    public void iGetResponseWithMatchingShopNames() throws Throwable {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("results", Matchers.hasSize(3)));
    }

    @Then("^I get response with no matching shop names$")
    public void iGetResponseWithNoShopNames() throws Throwable {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("total_results_count", is(0))
                .expectBody("results", Matchers.hasSize(0)));
    }

}