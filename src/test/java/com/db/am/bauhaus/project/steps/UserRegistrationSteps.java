package com.db.am.bauhaus.project.steps;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.bo.User;
import com.db.am.bauhaus.project.pages.HomePage;
import com.db.am.bauhaus.project.pages.UserRegistrationPage;
import com.db.am.bauhaus.project.util.GenericHelper;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class UserRegistrationSteps extends ScenarioSteps {


    private HomePage homePage;
    private UserRegistrationPage registrationPage;

    @Step
    public void registerUser() {
        homePage.goToUserRegistration();
        registrationPage.registerUser(GenericHelper.generateRandomUser());
    }

    @Step
    public void registerUser(User user) {
        homePage.goToUserRegistration();
        registrationPage.registerUser(user);
    }

    @Step
    public void verifySuccessfulRegistration() {
        assertThat(homePage.getRegistrationCompletionMessage1().equals("Hi, your account is unconfirmed."), is(true));
        assertThat(homePage.getRegistrationCompletionMessage2().contains(((User) SessionHelper.get(SessionVar
                .REGISTERED_USER)).getEmailId()), is(true));
    }

    public void verifyUserFieldValidation(Map<String, String> validations) {
        validations.forEach((key, value) -> {
                    assertThat("Validating to see if scenario "+key+" is "+value, registrationPage.validateErrorScenario(key, Boolean.valueOf(value)), is(true));
                }
        );
    }
}
