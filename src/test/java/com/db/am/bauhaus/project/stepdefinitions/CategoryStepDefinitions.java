package com.db.am.bauhaus.project.stepdefinitions;

import com.db.am.bauhaus.project.steps.CategorySteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by ongshir on 05/10/2016.
 */
public class CategoryStepDefinitions {

    @Steps
    CategorySteps categorySteps;

    @When("^he navigates to \"([^\"]*)\" and then to \"([^\"]*)\"$")
    public void navigateToSubCategory(String category, String subCategory) throws Throwable {
        categorySteps.navigateToSubCategory(category, subCategory);
    }

    @Then("^the products for the selected sub category should be displayed$")
    public void verifySubCategoryPageResults() throws Throwable {
        categorySteps.verifySubCategoryPageResults();
    }

    @Then("^when he selects product \"([^\"]*)\" from search results$")
    public void selectProduct(String productName) throws Throwable {
        categorySteps.selectProduct(productName);
    }

    @Then("^the corresponding product details should be displayed$")
    public void the_product_details_should_be_displayed() throws Throwable {
        categorySteps.verifyProductDetails();
    }
}
