package com.db.am.bauhaus.project.stepdefinitions;

import com.db.am.bauhaus.project.steps.CategorySteps;
import com.db.am.bauhaus.project.steps.SearchUserSteps;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

/**
 * Created by ongshir on 05/10/2016.
 */
public class SearchStepDefinitions {

    @Steps
    SearchUserSteps searchUserSteps;

    @Given("^John is viewing the Etsy landing page$")
    public void gotoLandingPage() {
        searchUserSteps.goToLandingPage();
    }

    @When("^he searches for \"([^\"]*)\" a product from the input box$")
    public void searchFromInputBox(String searchInput) throws Throwable {
        searchUserSteps.search_from_input_box(searchInput);
    }

    @Then("^the result should be displayed$")
    public void verifySearchResult() {
        //searchUserSteps.verify_result_for_top_categories();
        searchUserSteps.verify_result_for_all_categories();
    }

    @When("^he hovers over menu \"([^\"]*)\" then over submenu \"([^\"]*)\" and clicks on product category \"([^\"]*)\"$")
    public void menuHoverAndProductClick(String menu, String subMenu, String productCategory) throws Throwable {
        searchUserSteps.hoverOverAndClick(menu, subMenu, productCategory);
    }

    @Then("^he should be able to see the product results displayed$")
    public void verifyProductListDisplay() throws Throwable {
        searchUserSteps.verifyProductListDisplay();
    }

}
