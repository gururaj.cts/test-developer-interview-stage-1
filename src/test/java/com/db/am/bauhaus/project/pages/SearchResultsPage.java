package com.db.am.bauhaus.project.pages;


import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.util.GenericHelper;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

@DefaultUrl("/")
public class SearchResultsPage extends Page {

    @FindBy(css = ".display-inline.text-smaller")
    private WebElementFacade allCategoryHeader;

    @FindBy(css = ".ss-icon.ss-navigateright.icon-smaller-lg.icon-t-1")
    private WebElementFacade nextPageElement;

    @FindBy(css = ".mt-xs-2.pl-xs-1")
    private WebElementFacade breadcrumb;

    private static final By productListLocator = By.cssSelector(".v2-listing-card__info");

    public String getAllCategoriesHeader() {
        return allCategoryHeader.getText();
    }

    public void selectProduct(String productName) {

        //Getting all the products on current page
        List<WebElement> productElements = getDriver().findElements(productListLocator);

        //Looping through the products to find a matching product
        for(WebElement productElement : productElements){
            if(productElement.getText().contains(productName)){
                scrollAndClick(productElement);
                SessionHelper.set(productName, SessionVar.SELECTED_PRODUCT);
                return;
            }
        }

        //Product not found on this Page, moving to next page and trying our luck!
        goToNextPage();
        selectProduct(productName);
    }

    private void goToNextPage() {
        scrollAndClick(nextPageElement);
    }

    public String getBreadCrumb() { return breadcrumb.getText(); }

}
