package com.db.am.bauhaus.project.pages;


import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

@DefaultUrl("/")
public class HomePage extends Page {


    @FindBy(id = "register")
    private WebElementFacade registerLink;

    @FindBy(css = ".text-title-smaller.mb-xs-1")
    private WebElementFacade registrationMessage1;

    @FindBy(css = ".confirm-email-notice>p")
    private WebElementFacade registrationMessage2;

    @FindBy(id = "sign-in")
    private WebElementFacade signinLink;

    @FindBy(css = ".top-nav-item")
    List<WebElementFacade> menusElements;

    @FindBy(id = "search-query")
    WebElementFacade inputBox;

    @FindBy(css = ".btn.btn-primary")
    WebElementFacade searchButton;

    private static final By desktopCategoriesIdentifier = By.cssSelector(".vesta-hp-category-default a");
    private static final By mobileCategoriesIdentifier = By.cssSelector(".vesta-hp-category-so-mobile a");
    private static final String parentNodeIdentifierTemplate = "div[data-node-id='<DATA_NODE_ID_PLACE_HOLDER>']";
    private static final String productsCategoryIdentifierTemplate = "a[href*='<DATA_NODE_ID_PLACE_HOLDER>']";
    private static final String categoryTitle = "span.vesta-hp-category-title";
    private static final By searchSuggestions = By.cssSelector("#search-suggestions li");
    private static final String randomSearchSuggestion = "#search-suggestions li:nth-of-type(<ITEM>)";

    public void searchFromInputBox(String searchText) {
        enterSearchText(searchText);
        searchButton.click();
    }

    public void goToUserRegistration() {
        registerLink.click();
    }

    public void goToSignin() {
        signinLink.click();
    }

    public String getRegistrationCompletionMessage1() {
        return registrationMessage1.waitUntilVisible().getText();
    }

    public String getRegistrationCompletionMessage2() {
        return registrationMessage2.waitUntilVisible().getText();
    }

    public void navigateToCategory(String categoryName) {

        new WebDriverWait(getDriver(), 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver driver) -> {
                    for (WebElement category : findElements(desktopCategoriesIdentifier, mobileCategoriesIdentifier)){
                        if(categoryName.equalsIgnoreCase(category.findElement(By.cssSelector(categoryTitle)).getText())){
                            SessionHelper.set(categoryName, SessionVar.SELECTED_CATEGORY);
                            category.click();
                            return true;
                        }
                    }
                    return false;
                });
    }

    public void enterSearchText(String searchText){
        inputBox.waitUntilPresent().sendKeys(searchText);
    }

    public void hoverOver(String menu, String subMenu) throws Throwable{

        //Waiting for all the Top level Menu elements to be visible before starting the Interactions with them
        waitForAllTopLevelMenuToBeVisible();

        for (WebElementFacade menuElement : menusElements) {
            if(menu.equalsIgnoreCase(menuElement.getText())){
                String selectedDataNodeId = menuElement.getAttribute("data-node-id");

                SessionHelper.set(selectedDataNodeId, SessionVar.DATA_NODE_ID);
                hover(menuElement);

                //Waiting for the Menu hover to show up before selecting the sub menu
                waitForMenuDropdownToShowUpAfterHover(selectedDataNodeId);

                //Getting the Sub Menu elements based on the selected Menu
                WebElement subMenuParent = getDriver().findElement(By.cssSelector(parentNodeIdentifierTemplate.replace("<DATA_NODE_ID_PLACE_HOLDER>",selectedDataNodeId)));
                List<WebElement> subMenus = subMenuParent.findElements(By.cssSelector(".side-nav-item>span"));
                for (WebElement subMenuElement : subMenus) {
                    if(subMenu.equalsIgnoreCase(subMenuElement.getText())){
                        hover(subMenuElement);
                        return;
                    }
                }
            }
        }
    }

    private void waitForAllTopLevelMenuToBeVisible() {
        List<WebElement> topMenuElements = getDriver().findElements(By.cssSelector(".top-nav-item"));
        WebDriverWait w = new WebDriverWait(getDriver(), 5);
        w.until(ExpectedConditions.visibilityOfAllElements(topMenuElements));
    }

    private void waitForMenuDropdownToShowUpAfterHover(String selectedDataNodeId) {
        String identifier = "div[data-node-id='"+selectedDataNodeId+"']";

        WebElement menuElementDropDown = getDriver().findElement(By.cssSelector(identifier));
        WebDriverWait w = new WebDriverWait(getDriver(), 5);
        w.until(ExpectedConditions.attributeToBe(menuElementDropDown, "aria-hidden", "false"));
    }

    public void selectProductCategory(String productCategory) throws Throwable{
        String productCategoryIdentifier = productsCategoryIdentifierTemplate.replace("<DATA_NODE_ID_PLACE_HOLDER>",
                SessionHelper.get(SessionVar.DATA_NODE_ID));
        List<WebElement> productCategoryElements = getDriver().findElements(By.cssSelector(productCategoryIdentifier));
        for(WebElement productCategoryElement : productCategoryElements){
            if(productCategoryElement.getText().contains(productCategory)) {
                productCategoryElement.click();
                break;
            }
        }
    }

    private void hover(WebElement menuElement) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(menuElement).build().perform();
    }

}
