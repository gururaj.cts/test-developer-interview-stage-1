package com.db.am.bauhaus.project.pages;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.bo.User;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class SigninPage extends PageObject {

    @FindBy(id = "username-existing")
    private WebElementFacade emailId;

    @FindBy(id = "password-existing")
    private WebElementFacade passwordField;

    @FindBy(id = "signin-button")
    private WebElementFacade signIn;

    public void doSignin(String userName, String password) {
        emailId.sendKeys(userName);
        passwordField.sendKeys(password);
        signIn.click();
    }
}
