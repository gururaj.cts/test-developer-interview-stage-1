package com.db.am.bauhaus.project.steps;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.pages.HomePage;
import com.db.am.bauhaus.project.pages.SearchResultsPage;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

/**
 * Created by ongshir on 05/10/2016.
 */
public class SearchUserSteps extends ScenarioSteps {

    HomePage homePage;
    SearchResultsPage searchResultsPage;

    @Step
    public void search_from_input_box(String searchInput) {
        homePage.searchFromInputBox(searchInput);
        SessionHelper.set(searchInput, SessionVar.INPUT_SEARCH_TEXT);
    }

    @Step
    public void verify_result_for_all_categories() {
        assertThat(searchResultsPage.getAllCategoriesHeader(), containsString(SessionHelper.get(SessionVar.INPUT_SEARCH_TEXT)));
    }

    @Step
    public void goToLandingPage() {
        homePage.openAndResizeWindow();
    }

    @Step
    public void hoverOverAndClick(String menu, String subMenu, String productCategory) throws  Throwable{
        homePage.hoverOver(menu, subMenu);
        homePage.selectProductCategory(productCategory);
        SessionHelper.set(productCategory, SessionVar.SELECTED_CATEGORY);
    }

    @Step
    public void verifyProductListDisplay() {
        String productCategory = (String)SessionHelper.get(SessionVar.SELECTED_CATEGORY);
        assertThat(searchResultsPage.getBreadCrumb().contains(productCategory), is(true));
        assertThat(searchResultsPage.getTitle().contains(productCategory), is(true));
    }
}
