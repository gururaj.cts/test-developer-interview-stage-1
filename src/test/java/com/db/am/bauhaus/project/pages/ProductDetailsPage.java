package com.db.am.bauhaus.project.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

public class ProductDetailsPage extends Page {

    @FindBy(css = "#listing-page-cart-inner>h1>span")
    private WebElementFacade productName;

    public String getProductName(){
        return productName.getText();
    }
}
