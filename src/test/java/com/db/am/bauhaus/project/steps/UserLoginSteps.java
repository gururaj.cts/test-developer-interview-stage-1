package com.db.am.bauhaus.project.steps;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.pages.HomePage;
import com.db.am.bauhaus.project.pages.SigninPage;
import com.db.am.bauhaus.project.pages.YourAccountPage;
import com.db.am.bauhaus.project.util.SessionHelper;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class UserLoginSteps extends ScenarioSteps {

    private SigninPage signinPage;
    private YourAccountPage yourAccountPage;
    HomePage homePage;

    @Step
    public void doSignin(String userName, String password) {
        homePage.goToSignin();
        signinPage.doSignin(userName, password);
        SessionHelper.set(userName, SessionVar.LOGGED_IN_USER_ID);
    }

    @Step
    public void verifyAccountDetail(String accountName) {
        yourAccountPage.goToYourAccounts();
        assertThat(yourAccountPage.getAccountName().equals(accountName), is(true));
    }
}
