package com.db.am.bauhaus.project.pages;

import com.db.am.bauhaus.project.SessionVar;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SubCategoryPage extends Page{

    @FindBy(css = ".mt-xs-2.pl-xs-1")
    private WebElementFacade breadcrumb;

    @FindBy(css = ".float-left>h1")
    private WebElementFacade subCategoryHeader;

    public String getBreadCrumb() { return breadcrumb.getText(); }

    public String getSubCategoryHeader(){
        return subCategoryHeader.getText();
    }

}
