package com.db.am.bauhaus.project.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Page extends PageObject {

    public  List<WebElement> findElements(By... identifiers) {
        for (By identifier : identifiers) {
            WebDriverWait wait = new WebDriverWait(getDriver(), 1);
            try {
                return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(identifier));
            } catch (Exception toEx) {
                //Do nothing and continue with the next identifier
            }
        }
        throw new TimeoutException("No matching Visible elements found for the list of input Identifiers");
    }

    public void openAndResizeWindow(){
        open();
        resizeWindow(System.getProperty("Device.Id"));
    }

    public void scrollAndClick(WebElement webElement) {
        Point hoverItem = webElement.getLocation();
        clickWithinElementWithXYCoordinates(webElement, hoverItem.getX(), hoverItem.getY());
        webElement.click();
    }

    private void clickWithinElementWithXYCoordinates(WebElement webElement, int x, int y) {
        Actions builder = new Actions(getDriver());
        builder.moveToElement(webElement, x, y);
        builder.perform();
    }

    private void resizeWindow(String deviceId){
        if(deviceId == null) {
            maximizeWindow();
            return;
        }
        switch(deviceId){
            case "1":{
                System.out.println("Selected Device:IPhone 6");
                setWindowSize(375,667);
                break;
            }case "2":{
                System.out.println("Selected Device:Galaxy S5");
                setWindowSize(360,640);
                break;
            }case "3":{
                System.out.println("Selected Device:IPad");
                setWindowSize(768,1024);
                break;
            }default:{
                maximizeWindow();
            }
        }
    }

    private void maximizeWindow(){
        getDriver().manage().window().maximize();
    }

    private void setWindowSize(int width, int height){
        getDriver().manage().window().setSize(new Dimension(width,height));
    }
}
