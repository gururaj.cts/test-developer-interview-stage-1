package com.db.am.bauhaus.project.stepdefinitions;

import com.db.am.bauhaus.project.steps.UserLoginSteps;
import com.db.am.bauhaus.project.steps.UserRegistrationSteps;
import com.db.am.bauhaus.project.util.GenericHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.Map;

public class UserAccountStepDefinitions {

    @Steps
    UserRegistrationSteps userRegistrationSteps;

    @Steps
    UserLoginSteps userLoginSteps;

    @When("^he submits details for account registration$")
    public void registerUserAccount() {
        userRegistrationSteps.registerUser();
    }

    @Then("^he should see the registration success message$")
    public void verifySuccessfulRegistration() {
        userRegistrationSteps.verifySuccessfulRegistration();
    }

    @When("^he submits \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" for account registration$")
    public void he_submits_for_account_registration(String firstName, String email, String password, String repeatPassword) throws Throwable {
        userRegistrationSteps.registerUser(GenericHelper.populateUser(firstName, email, password, repeatPassword));
    }

    @Then("^he should see field validation errors$")
    public void he_should_see_field_validation_errors(Map<String, String> validations) throws Throwable {
        userRegistrationSteps.verifyUserFieldValidation(validations);
    }

    @When("^he logs into his account using user name \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void he_logs_into_his_account_using_user_name_and_password(String userName, String password) throws Throwable {
        userLoginSteps.doSignin(userName, password);
    }

    @Then("^\"([^\"]*)\" should be able to see his account details$")
    public void should_be_able_to_see_his_account_details(String accountName) throws Throwable {
        userLoginSteps.verifyAccountDetail(accountName);
    }
}
