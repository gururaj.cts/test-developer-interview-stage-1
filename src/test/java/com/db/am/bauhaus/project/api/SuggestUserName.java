package com.db.am.bauhaus.project.api;

import com.db.am.bauhaus.project.util.APIHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;

import static org.hamcrest.core.Is.is;

public class SuggestUserName extends APIWrapper {


    @Given("^Suggest user name API is available$")
    public void apiToSuggestUserNameIsAvailable() {
        buildUriSpec(APIHelper.getSuggestUsernameEndPoint());
    }


    @When("^I make user name suggestion request for email id \"([^\"]*)\"$")
    public void iMakeUsernameSuggestionRequest(String emailId) throws Throwable {
        addRequestParam("email", emailId);
        callService();
    }

    @Then("^I get response with username suggestions containing \"([^\"]*)\"$")
    public void iGetResponseWithUsernameSuggestions(String userNamePattern) throws Throwable {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("suggestions.suggestion", Matchers.hasSize(2))
                .expectBody("suggestions.suggestion[0]", Matchers.startsWith(userNamePattern))
                .expectBody("suggestions.suggestion[1]", Matchers.startsWith(userNamePattern)));
    }

    @Then("^I get a failure response$")
    public void iGetAFailureResponse() throws Throwable {
        assertStatusCode(400);
        assertResponseBody(getResponseSpecBuilder().expectBody("error", is("Missing input parameter: [email]")));
    }
}