package com.db.am.bauhaus.project.api;

import com.db.am.bauhaus.project.util.APIHelper;
import com.db.am.bauhaus.project.util.GenericHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.core.Is.is;

public class CheckUserNameAvailability extends APIWrapper{


    @Given("^User name checker API is available$")
    public void api_to_check_if_username_is_available() {
        buildUriSpec(APIHelper.getCheckUsernameAvailabilityEndPoint());
    }

    @When("^I check username \"([^\"]*)\" for availability$")
    public void iCheckUserName(String usernameToCheck) {
        addRequestParam("username", usernameToCheck);
        callService();
    }

    @Then("^I get response that username is un available$")
    public void iCheckIfUserNameIsUnAvailable() {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("usernameIsAvailable", is(false)));
    }

    @Then("^I get response that username is available$")
    public void iCheckIfUserNameIsAvailable() {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("usernameIsAvailable", is(true)));
    }

    @When("^I check a random user name for availability$")
    public void iCheckARandomlyUserNameForAvailability() {
        addRequestParam("username", String.valueOf(GenericHelper.getRandomString(10)));
        callService();
    }

    @When("^I get a response stating that username cannot be blank$")
    public void iGetResponseThatUserNameCannotBeBlank() {
        assertStatusCode(200);
        assertResponseBody(getResponseSpecBuilder().expectBody("usernameIsAvailable", is(false)).expectBody("message", is("Can't be blank.<br/>")));
    }

}



