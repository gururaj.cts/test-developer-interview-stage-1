package com.db.am.bauhaus.project.pages;

import com.db.am.bauhaus.project.SessionVar;
import com.db.am.bauhaus.project.util.SessionHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoryPage extends Page{

    private static final By desktopSubCategories_FourProducts = By.cssSelector(".block-grid-xl-4.show-xl a.category-text div");
    private static final By desktopSubCategories_ThreeProducts = By.cssSelector(".block-grid-xl-3.show-xl a.category-text div");
    private static final By mobileSubCategories = By.cssSelector(".block-grid-xs-2.show-xs  a.category-text div");

    public void navigateToSubCategory(String subCategoryName) throws InterruptedException {

        new WebDriverWait(getDriver(), 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver driver) -> {
                    for (WebElement subCategory : findElements(desktopSubCategories_FourProducts, desktopSubCategories_ThreeProducts, mobileSubCategories)){
                        if(subCategoryName.equalsIgnoreCase(subCategory.getText())){
                            SessionHelper.set(subCategoryName, SessionVar.SELECTED_SUBCATEGORY);
                            subCategory.click();
                            return true;
                        }
                    }
                    return false;
                });
    }
}
