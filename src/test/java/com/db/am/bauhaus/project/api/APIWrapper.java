package com.db.am.bauhaus.project.api;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.jayway.restassured.specification.ResponseSpecification;

import static com.jayway.restassured.RestAssured.given;

public class APIWrapper {

    private ResponseSpecBuilder responseSpecBuilder;
    private RequestSpecBuilder requestSpecBuilder;
    private RequestSpecification requestSpecification;
    private Response response;

    public APIWrapper() {
        responseSpecBuilder = new ResponseSpecBuilder();
        requestSpecBuilder = new RequestSpecBuilder();
    }

    public ResponseSpecBuilder getResponseSpecBuilder() {
        return responseSpecBuilder;
    }

    public void callService() {
        response = given()
                .spec(requestSpecification)
                .when()
                .get();
    }

    public void buildUriSpec(String baseURI) {
        requestSpecBuilder.setBaseUri(baseURI);
    }

    public void addRequestParam(String key, String value){
        requestSpecBuilder.addParam(key, value);
        requestSpecification = requestSpecBuilder.build();
    }

    public void assertStatusCode(int statusCode) {
        responseSpecBuilder.expectStatusCode(statusCode);
    }

    public void assertResponseBody(ResponseSpecBuilder responseSpecBuilder) {
        ResponseSpecification responseSpecification = responseSpecBuilder.build();
        response.then().spec(responseSpecification);
    }

}
