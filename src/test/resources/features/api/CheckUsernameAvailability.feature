Feature: Tests related to Check User name availability API

  Background:
    Given User name checker API is available

  @api @checkusername
  Scenario: Verify user name availability service for existing user name
    When I check username "Mark" for availability
    Then I get response that username is un available

  @api @checkusername
  Scenario: Verify user name availability service for new user name
    When I check a random user name for availability
    Then I get response that username is available

  @api @checkusername
  Scenario: Verify user name availability service for empty username
    When I check username "" for availability
    Then I get a response stating that username cannot be blank

