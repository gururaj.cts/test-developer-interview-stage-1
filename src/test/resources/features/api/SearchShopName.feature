Feature: Tests related to Shop name search API

  Background:
    Given Search shop name API is available

  @api @searchshopname
  Scenario: Verify to see if the service returns shop names for valid product name input
    When I make shop name search for input product "books"
    Then I get response with valid shop name results

  @api @searchshopname
  Scenario: Verify is the service doesn't return shop names for invalid product name input
    When I make shop name search for input product "fgdshgugddd"
    Then I get response with no matching shop names
