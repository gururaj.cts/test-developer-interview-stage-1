Feature: Tests related to Suggest username API

  Background:
    Given Suggest user name API is available

  @api @suggestusername
  Scenario: Verify is the service returns username suggestions for a valid input email
    When I make user name suggestion request for email id "gvishnum24@gmail.com"
    Then I get response with username suggestions containing "gvishnum"
#
  @api @suggestusername
  Scenario: Verify is the service returns username suggestions for a valid input email
    When I make user name suggestion request for email id ""
    Then I get a failure response