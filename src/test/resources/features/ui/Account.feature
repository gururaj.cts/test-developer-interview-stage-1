Feature: User Account registration and login feature

#  @ui @registration
#  Scenario: New Users should be able to register themselves
#    Given John is viewing the Etsy landing page
#    When he submits details for account registration
#    Then he should see the registration success message

  @ui @registration
  Scenario Outline: Validation of Input fields on Registration page
    Given John is viewing the Etsy landing page
    When he submits "<firstName>", "<email>", "<password>", "<repeatPassword>" for account registration
    Then he should see field validation errors
      | firstNameBlank       | <errorFirstNameBlank>       |
      | emailBlank           | <errorEmailNameBlank>       |
      | passwordBlank        | <errorPasswordBlank>        |
      | reenterPasswordBlank | <errorReenterPasswordBlank> |
    Examples:
      | firstName      | email          | password   | repeatPassword | errorFirstNameBlank | errorEmailNameBlank | errorPasswordBlank | errorReenterPasswordBlank |
      |                |                |            |                | true                | true                | true               | true                      |
      |                | test@gmail.com | password-1 | password-1     | true                | false               | false              | false                     |
      | dummyFirstName |                | password-1 | password-1     | false               | true                | false              | false                     |
      | dummyFirstName | test@gmail.com |            | password-1     | false               | false               | true               | true                      |
      | dummyFirstName | test@gmail.com | password-1 |                | false               | false               | false              | true                      |


  @ui @login
  Scenario: User should be able to login to check his Accounts
    Given John is viewing the Etsy landing page
    When he logs into his account using user name "gvishnum23@gmail.com" and password "password-1"
    Then "John" should be able to see his account details