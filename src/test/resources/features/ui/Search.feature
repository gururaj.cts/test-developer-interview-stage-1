Feature: Product Search feature

  @ui @search
  Scenario Outline: Should be able to search for a product from the input box
    Given John is viewing the Etsy landing page
    When he searches for "<search_input>" a product from the input box
    Then the result should be displayed
    Examples:
      | search_input |
      | craft        |
      | books        |

  @ui @search
  Scenario Outline: Should be able to search for a product from the drop-down menu new
    Given John is viewing the Etsy landing page
    When he hovers over menu "<Menu>" then over submenu "<Sub Menu>" and clicks on product category "<Product Category>"
    Then he should be able to see the product results displayed
    And when he selects product "<Product>" from search results
    Then the corresponding product details should be displayed
    Examples:
      | Menu                    | Sub Menu     | Product Category | Product                  |
      | Jewellery & Accessories | RINGS        | Bands            | 9ct gold solid fine ring |
      | Home & Living           | PET SUPPLIES | Pet Bedding      | Dog Blanket              |

  @ui @search @responsive
  Scenario Outline: Should be able to search for a product from the icons
    Given John is viewing the Etsy landing page
    When he navigates to "<Category>" and then to "<Sub Category>"
    Then the products for the selected sub category should be displayed
    And when he selects product "<Product>" from search results
    Then the corresponding product details should be displayed
    Examples:
      | Category      | Sub Category | Product      |
      | Weddings      | Jewellery    | Wedding Ring |
      | Home & Living | Bedding      | Cot Bumper   |